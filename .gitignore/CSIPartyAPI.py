#!flask/bin/python
from flask import Flask
from flask import request
import threading
import datetime
import time
import logging

app = Flask(__name__)

logging.basicConfig(filename='CSIPartyAPI.log', filemode='w', level=logging.DEBUG)
transPerSec = 30	# max transactions per second
procTime = 300		# processing time in ms
lock = threading.Lock()
lastStart = datetime.datetime.now()

print("Start stub for CSIPartyAPI at %s" % (str(datetime.now())))

@app.route('/CSIPartyAPI/<CSI_id>/<status>', methods=['POST'])
def CSIPartyAPI(CSI_id, status):
    if request.method == 'POST':
		global lastStart
		global lock
		lock.acquire()
		delay = lastStart + 1 / transPerSec - datetime.datetime.now()
		lock.release()
		if delay > 0:
			time.sleep(delay)
		lock.acquire()
		lastStart = datetime.datetime.now()
		lock.release()

		print('Request to CSIPartyAPI, CSI_id: %s, status: %s (%d)' % (CSI_id, status))
		logging.info('Request to CSIPartyAPI, CSI_id: %s, status: %s (%d)' % (CSI_id, status))
		time.sleep(procTime / 1000)
		print('Request acknowledged')
		return 'ack'

if __name__ == '__main__':
	app.run(debug=True)
