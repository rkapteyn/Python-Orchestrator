#!flask/bin/python
from flask import Flask
from flask import request
import threading
import time
import logging

app = Flask(__name__)

logging.basicConfig(filename='MDM.log', filemode='w', level=logging.DEBUG)

lock = threading.Lock()
counter = 1

print("Stub for Migration Status Indicator")
@app.route('/setMigrationStatusIndicator/<customerNumber>/<status>', methods=['POST'])
def setMigrationStatusIndicator(customerNumber, status):
    if request.method == 'POST':
		global counter
		global lock
		lock.acquire()
		print('Request to set Migration Status Indicator of customer %s to status %s (%d)' % (customerNumber, status, counter))
		logging.info('MigrationStatusIndicator, setMigrationStatusIndicator, %s, %s' % (customerNumber, status))
		counter = counter + 1
		lock.release()
		time.sleep(0.4)
		print('Request acknowledged')
		return 'ack'

if __name__ == '__main__':
	app.run(debug=True)
