#####################################################################################
# Migration Orchestrator
# version 0.1
# 15-sep-18
# Ruud Kapteijn
#####################################################################################

import pandas as pd
from datetime import datetime
import requests
import threading
import time
import logging


def setMigrationStatusIndicator(customerNumber, status):
    print("Send request setMigrationStatusIndicator(%s, %s) at %s" % (customerNumber, status, str(datetime.now())))
    url = 'http://127.0.0.1:5000/setMigrationStatusIndicator/' + str(customerNumber) + '/"' + status + '"'
    r = requests.post(url)
    print("Reply %s received at %s" % (r.text, str(datetime.now())))


print("Start orchestrator at %s" % (str(datetime.now())))
logging.basicConfig(filename='orchestrator.log', filemode='w', level=logging.INFO)
logging.info('Start orchestrator')

# open sample.csv contains 2,500 cluster numers of sample
clusters = pd.read_csv("sample.csv")
customers = pd.read_csv("cus.csv")
agreements = pd.read_csv("agr.csv")
print("%d clusters in sample" % (len(clusters.index)))

custCnt = 0
agrCnt = 0
maxCust = 0
maxAgr = 0
clusterCnt = 0

for index, row in clusters.iterrows():
    # get customers
    print("migrate cluster %d" % (row['cluster']))
    custSet = customers.loc[customers['CLUSTER'] == row['cluster']]
    custCnt += len(custSet.index)
    if len(custSet.index) > maxCust:
        maxCust = len(custSet.index)

    # get agreements
    agrSet = agreements.loc[agreements['CLUSTER'] == row['cluster']]
    agrCnt += len(agrSet.index)
    if len(agrSet.index) > maxAgr:
        maxAgr = len(agrSet.index)

    for index, row in custSet.iterrows():
        print("migrate customer %d" % row['N_PERS'])

    for index, row in agrSet.iterrows():
        print("migrate agreement %d" % row['PK_CONTRACT_ID'])

    clusterCnt += 1
    if clusterCnt >= 5:
        break

print("%d customers, maximum cluster size: %d customers" % (custCnt, maxCust))
print("%d agreements, maximum cluster size: %d agreements" % (agrCnt, maxAgr))

# # Create new threads
# threads = []
#
# logging.info('setMigrationStatusIndicator, 1000, In migration')
# t = threading.Thread(target=setMigrationStatusIndicator, args=(1000, "In migration",))
# threads.append(t)
# t.start()
#
# logging.info('setMigrationStatusIndicator, 1001, In migration')
# t = threading.Thread(target=setMigrationStatusIndicator, args=(1001, "In migration",))
# threads.append(t)
# t.start()
#
# # wait until threads have ended
# for t in threads:
#     t.join()
# logging.info('setMigrationStatusIndicator completed')

logging.info('End orchestrator')
print("End orchestrator at %s" % (str(datetime.now())))

