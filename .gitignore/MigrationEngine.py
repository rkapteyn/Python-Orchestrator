#!flask/bin/python
from flask import Flask
from flask import request
import time

app = Flask(__name__)

print("Stub for Migration Engine")
@app.route('/migrateAccount/<accountNumber>', methods=['POST'])
def migrateAccount(accountNumber):
    if request.method == 'POST':
		print('Request to migrate account %s' % (accountNumber))
		time.sleep(2.5)
		print('Request acknowledged')
		return 'ack'

if __name__ == '__main__':
    app.run(port=5001, debug=True)
